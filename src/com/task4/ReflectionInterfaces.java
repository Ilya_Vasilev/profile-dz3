package com.task4;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ReflectionInterfaces {
    public static void main(String[] args) {
        System.out.println(getAllInterfaces(E.class));
    }

    public static Set<Class<?>> getAllInterfaces(Class<?> cls) {
        Set<Class<?>> interfaces = new HashSet<>();
        while (cls != Object.class) {
            Set<Class<?>> tmp = new HashSet<>(List.of(cls.getInterfaces()));
            for (Class<?> interfaceCls : tmp) {
                Class<?> checkedInterface = interfaceCls;
                while (checkedInterface != null) {
                    tmp.addAll(List.of(checkedInterface.getInterfaces()));
                    checkedInterface = checkedInterface.getSuperclass();
                }
            }
            interfaces.addAll(tmp);
            cls = cls.getSuperclass();
        }
        return interfaces;
    }
}
