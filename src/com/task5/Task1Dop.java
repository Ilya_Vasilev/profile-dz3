package com.task5;

public class Task1Dop {
    public static void main(String[] args) {
        System.out.println(bracketSequence(""));
        System.out.println(bracketSequence("(())"));
        System.out.println(bracketSequence("()())"));
        System.out.println(bracketSequence("(()))"));
        System.out.println(bracketSequence(")("));
        System.out.println(bracketSequence("("));
        System.out.println(bracketSequence("((()))"));

    }

    public static boolean bracketSequence(String str) {
        char[] arr = str.toCharArray();
        int count = 0;

        if (str.isEmpty()) {
            return true;
        }
        for (char el : arr) {
            if (el == '(') {
                count++;
            } else if (el == ')') {
                count--;
            }
            if (count < 0) {
                return false;
            }
        }
        return count == 0;
    }
}
